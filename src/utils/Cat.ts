import Axios from 'axios';
import { getUser } from './User';

export interface CatDetailProps {
  id: string
  url: string
  width: number
  height: number
  mime_type: string
  breeds: { id: number, name: string } []
  categories: string[]
  breed_ids: string[]
  favourited?: string
}

export const toggleFavouriteStatus = ({catId, favId}: {catId?: string, favId?: string}) => {
  if (favId) {
    Axios.delete(`/favourites/${favId}`)
  } else if(catId) {
    Axios.post('/favourites', {
        image_id: catId,
        sub_id: getUser()
    })    
  }
  else {
    console.log("Toggling favorite failed spectacularly.")
  }
}