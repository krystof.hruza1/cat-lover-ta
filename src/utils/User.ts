
//For time reasons, users are not strictly unique, but rather random
export const getUser = (): string => {
  const ls = window.localStorage
  const user = ls.getItem('user')
  if (user) {
    return user;
  }
  else {
    const newUser = String(Math.round(Math.random() * 1000))
    ls.setItem('user', newUser)
    return newUser
  }
}