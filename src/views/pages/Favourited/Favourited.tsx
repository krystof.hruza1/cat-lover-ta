import React, { useEffect, useState } from 'react'
import Axios from 'axios'
import Image from 'react-bootstrap/Image'
import "./style.scss"
import { Link } from 'react-router-dom'
import { getUser } from '../../../utils/User'
import { Button } from 'react-bootstrap'
import { toggleFavouriteStatus } from '../../../utils/Cat'
import Heart from '../../components/Icon/Heart'

export interface FavouritedProps {
  id: string
  image_id: string
  sub_id: string
  image: {id: string, url: string}
}

const Favourited = ({ showModal }: { showModal?: boolean }) => {
  const [cats, updateCats] = useState([]) as any[]
  const uri = "https://api.thecatapi.com/v1/favourites"
  const getFavouriteCats = () => {
    Axios.get(uri, {
      params: {
        sub_id: getUser(),
        attach_image: true
      }
    })
      .then((result) => {
      updateCats([...cats, ...result.data])
    })
}
  // Only run once
  // eslint-disable-next-line react-hooks/exhaustive-deps
  useEffect(() => getFavouriteCats(), [])

  return (
    <div id="fav-list">
      <div className="list-wrapper">
        {cats.map((fav: FavouritedProps, i: number) => {
          return (
            <div className="item">
              <Link to={`/cats/${fav.id}`} key={i} >
                <Image src={fav.image.url} thumbnail roundedCircle />
              </Link>
              <div className="controls-wrapper">
                <Button className="fav-btn" onClick={() => toggleFavouriteStatus({ catId: fav.image_id, favId: fav.id })}>
                  <Heart />
                </Button>
              </div>
            </div>
          )
        })}
      </div>
    </div>
  )
}

export default Favourited