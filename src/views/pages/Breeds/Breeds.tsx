import React, { useEffect, useState } from 'react'
import Axios from 'axios'
import "./style.scss"
import { Link } from 'react-router-dom'
import BreedModal from '../../components/BreedModal/BreedModal'

export interface BreedProps {
  id: string
  name: string
}

const Breeds = ({ showModal }: { showModal?: boolean }) => {
  const [breeds, updateBreeds] = useState([]) as any[]
  const [showModalOverride, setShowModalOverride] = useState(showModal)
  const uri = "https://api.thecatapi.com/v1/breeds"
  const getMoreCats = () => {
    Axios.get(uri)
      .then((result) => {
      updateBreeds(result.data)
    })
}
  // Only run once
  // eslint-disable-next-line react-hooks/exhaustive-deps
  useEffect(() => getMoreCats(), [])

  return (
    <div id="breeds-list">
      <div className="list-wrapper">
        {breeds.map((breed: BreedProps, i: number) => {
          return (
            <Link to={`/breeds/${breed.id}`} onClick={() => setShowModalOverride(true)} key={i}>{breed.name}</Link>
          )
        })}
      </div>
      {
        showModal && <BreedModal hide={!showModalOverride} onHide={setShowModalOverride} />
      }
    </div>
  )
}

export default Breeds