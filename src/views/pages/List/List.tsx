import React, { useEffect, useState } from 'react'
import Axios from 'axios'
import Image from 'react-bootstrap/Image'
import "./style.scss"
import { Link } from 'react-router-dom'
import CatModal from '../../components/CatModal/CatModal'
export interface CatProps {
  id: string
  url: string
  width: number
  height: number
  mime_type: "image/jpeg"
  breed: any[]
  categories: any[]
  breed_ids: any[]
}

const List = ({ showModal }: { showModal?: boolean }) => {
  const [cats, updateCats] = useState([]) as any[]
  const [showModalOverride, setShowModalOverride] = useState(showModal)
  const uri = "https://api.thecatapi.com/v1/images/search"
  const getMoreCats = () => {
    Axios.get(uri, {
      params: {
        limit: 10
      }
    })
      .then((result) => {
      updateCats([...cats, ...result.data])
    })
}
  // Only run once
  // eslint-disable-next-line react-hooks/exhaustive-deps
  useEffect(() => getMoreCats(), [])

  return (
    <div id="cats-list">
      <div className="list-wrapper">
        {cats.map((cat: CatProps, i: number) => {
          return (
            <Link to={`/cats/${cat.id}`} key={i} onClick={() => setShowModalOverride(true)} >
              <Image src={cat.url} />
            </Link>
          )
        })}
      </div>
      <div className="controls-wrapper">
        <button onClick={(e) => getMoreCats()}>More cats</button>
      </div>
      {
        showModal && <CatModal hide={!showModalOverride} onHide={setShowModalOverride} />
      }
    </div>
  )
}

export default List