import Axios from 'axios'
import React, { Dispatch, SetStateAction, useEffect, useState } from 'react'
import { Image, Modal } from 'react-bootstrap'
import { Link, useParams } from 'react-router-dom'
import { CatDetailProps } from '../../../utils/Cat'
import "./style.scss"

export interface CatModalProps {
  hide?: boolean
  onHide?: Dispatch<SetStateAction<boolean | undefined>>
}

export interface BreedDetailProps {
  id: string
  url: string
  width: number
  height: number
}

const BreedModal = (params: CatModalProps) => {
  const [data, setData] = useState(
    [{
      id: "",
      url: "",
      width: 0,
      height: 0,
      mime_type: "",
      breeds: [],
      categories: [],
      breed_ids: []
    }] as CatDetailProps[])

  const urlParams = useParams()
  useEffect(() => {
    const uri = `/images/search`
    Axios.get(uri, {
      params: {
        breed_ids: urlParams.id,
        limit: 2, //Seems to not actually work
        api_key: process.env.CAT_API_KEY
      }
    })
      .then((result) => {
        setData(result.data)
      })
  }, [urlParams.id])

  const [show, setShow] = useState(!params.hide)
  useEffect(() => setShow(!params.hide), [params.hide])
  const handleClose = () => {
    params.onHide && params.onHide(false)
    setShow(false)
  }

  return (
    <Modal show={show} onHide={() => handleClose()} id="breed-modal">
      <Modal.Header closeButton>
        <Modal.Title>Breed modal</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="content-wrapper">
          {data && data.map((item: BreedDetailProps, i: number) => {
            return (
              <Link to={`/cats/${item.id}`} key={i}>
                <Image src={item.url} />
              </Link>
            )
          })
          }
        </div>
      </Modal.Body>
    </Modal>
  )
}

export default BreedModal