import Axios from 'axios'
import React, { Dispatch, SetStateAction, useEffect, useMemo, useState } from 'react'
import { Badge, Button, Image, Modal } from 'react-bootstrap'
import { Link, useParams } from 'react-router-dom'
import { CatDetailProps, toggleFavouriteStatus } from '../../../utils/Cat'
import { getUser } from '../../../utils/User'
import Heart from '../Icon/Heart'
import "./style.scss"

export interface CatModalProps {
  hide?: boolean
  onHide?: Dispatch<SetStateAction<boolean | undefined>>
}



const CatModal = (params: CatModalProps) => {
  const [data, setData] = useState(
    {
      id: "",
      url: "",
      width: 0,
      height: 0,
      mime_type: "",
      breeds: [],
      categories: [],
      breed_ids: []
    } as CatDetailProps)

  const urlParams = useParams()

  const user = useMemo(() => getUser(), [])

  useEffect(() => {
    const uri = `/images/${urlParams.id}`
    Axios.get(uri, {
      params: {
        sub_id: user,
        include_favourite: true
      }
    })
      .then((result) => {
        setData(result.data)
        console.log(result.data)
      })
  }, [urlParams.id, user])

  const [show, setShow] = useState(!params.hide)
  useEffect(() => setShow(!params.hide), [params.hide])
  const handleClose = () => {
    params.onHide && params.onHide(false)
    setShow(false)
  }

  return (
    <Modal show={show} onHide={() => handleClose()} id="cat-modal">
      <Modal.Header closeButton>
        <Modal.Title>Cat modal</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="content-wrapper">
          {data
            ? (<>
              <Image src={data.url} />
              <div className="description-wrapper">
                <div className="breeds-wrapper">
                  {data.breeds
                    ? (data.breeds.map(breed => (
                      <Link to={`/breeds/${breed.id}`}>
                        <Badge bg="secondary">{breed.name}</Badge>
                      </Link>
                    )))
                    : null
                  }
                </div>
                <div className="controls-wrapper">
                  <Button variant="outline-danger" onClick={() => toggleFavouriteStatus({ catId: data.id, favId: data.favourited })}>
                    <Heart />
                  </Button>
                </div>
              </div>
            </>) : null

          }
        </div>
      </Modal.Body>
    </Modal>
  )
}

export default CatModal