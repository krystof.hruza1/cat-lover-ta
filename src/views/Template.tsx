import React from 'react'
import { Outlet } from "react-router-dom";
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import Container from 'react-bootstrap/Container'
import "./style.scss"
function Template() {

  const key = window.location.pathname

  return (
    <div id="app-main">
      <Navbar bg="dark" expand="lg" variant="dark">
        <Container>
          <Navbar.Brand href="/">Cat lover</Navbar.Brand>
          <Nav activeKey={key}>
            <Nav.Item><Nav.Link href="/cats">List</Nav.Link></Nav.Item>
            <Nav.Item><Nav.Link href="/breeds">Breeds</Nav.Link></Nav.Item>
            <Nav.Item><Nav.Link href="/favourited">Favourited</Nav.Link></Nav.Item>
          </Nav>
        </Container>

      </Navbar>
      <div className="main-container">
        <Outlet />
      </div>
      
    </div>
  )
}

export default Template