import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import ReactDOM from 'react-dom/client';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import Template from './views/Template';
import List from './views/pages/List/List';
import Favourited from './views/pages/Favourited/Favourited';
import Breeds from './views/pages/Breeds/Breeds';
import Axios from 'axios';
const router = createBrowserRouter([
  {
    path: "/",
    element: <Template key="" />,
    children: [
      {
        path: "cats/:id",
        element: <List showModal />
      },
      {
        path: "cats",
        element: <List />
      },
      {
        path: "favourited",
        element: <Favourited />
      },
            {
        path: "breeds/:id",
        element: <Breeds showModal />
      },
      {
        path: "breeds",
        element: <Breeds />
      }
    ],
  },
]);

Axios.defaults.headers.common = {
  'x-api-key': process.env.REACT_APP_CAT_API_KEY
}
Axios.defaults.baseURL = "https://api.thecatapi.com/v1"

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);