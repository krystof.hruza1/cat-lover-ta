# Test assignment commentary

## Time limitations
Due to time limitations, certain things were left unimplemented or only sketched, namely:
 - UX considerations as a whole
 - UI beyond minimal requirements
 - Systematic approach to fetching data
 - Checking whether a cat is already marked as a favourite
 - Actually unique users
 - Non glitchy, more sensible modals
 - Translations